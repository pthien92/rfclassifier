/**
 * Created by pthien92 on 22/09/15.
 */

import weka.classifiers.Evaluation;
import weka.core.converters.ConverterUtils.DataSource;
import weka.core.Instances;
import weka.classifiers.Classifier;
import weka.classifiers.trees.RandomForest;
import java.util.Scanner;
import weka.core.SerializationHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class RFClassifier {
    protected static Classifier m_classifier = null;
    protected static Instances train = null;
    protected static Instances test = null;
    RFClassifier() {
        super();
    }
    public void setTraining(String name) throws Exception {
        String filename = name;
        DataSource source_train = new DataSource(filename);
        train = source_train.getDataSet();
        if (train.classIndex() == -1) {
            train.setClassIndex(train.numAttributes()-1);
        }
    }
    public void setTesting(String name) throws Exception {
        String filename = name;
        DataSource source_test = new DataSource(filename);
        test = source_test.getDataSet();
        if (test.classIndex() == -1) {
            test.setClassIndex(test.numAttributes()-1);
        }
    }
    public void setClassifier(String[] Options) throws Exception {
        m_classifier = new RandomForest();
        m_classifier.setOptions(Options);
    }
    public void buildTrainer(Instances train) throws Exception {
        m_classifier.buildClassifier(train);
    }
    public void saveModel(String name) throws Exception {
        Object[] model_and_data = new Object[2];
        model_and_data[0] = m_classifier;
        model_and_data[1] = train;
        weka.core.SerializationHelper.writeAll(name, model_and_data);
    }
    public void loadModel(String name) throws Exception {
        Object[] model_and_data;
        model_and_data = weka.core.SerializationHelper.readAll(name);
        m_classifier = (Classifier) model_and_data[0];
        train = (Instances) model_and_data[1];
    }
    public static void main (String[] args) throws Exception {
        RFClassifier cls = new RFClassifier();
        String training_filename = "../Data_A_training.arff";
        String testing_filename = "../Data_B_training.arff";
        cls.setTraining(training_filename);
        cls.setTesting(testing_filename);
        String[] options = weka.core.Utils.splitOptions("-I 15 -K 0 -S 1");
        cls.setClassifier(options);
        cls.buildTrainer(train);
        Evaluation eval = new Evaluation(train);
        eval.evaluateModel(cls.m_classifier, test);
        cls.saveModel("../Data_A.model");
        System.out.println(eval.toSummaryString("\nResults\n==========\n", false));
        System.out.println(eval.toClassDetailsString("Score table\n==========\n"));
        System.out.println(eval.toMatrixString("Confusion Matrix\n===============\n"));

    }
}
