(c) Thien Pham 2015

RandomForest Classifier using WEKA API
=========================================

- Take advantage of WEKA API to quickly construct a Machine learning classifier (Random Forest for instance)
- RFClassifier: load the dataset (ARFF format) as the training set, after the classifier was trained, a model is saved
- Detector: load the saved model then uses it to predict the testing set

TODO
====
- Parsing arguments techniques
- Handle multiple input files
- Work continously in real time scenario such as periodically checking for new collected datasets and automatically testing them

