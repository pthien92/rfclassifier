/**
 * Created by pthien92 on 22/09/15.
 */
import weka.classifiers.Evaluation;
import weka.core.converters.ConverterUtils.DataSource;
import weka.core.Instances;
import weka.classifiers.Classifier;

public class Detector {
    protected static Classifier m_classifier = null;
    protected static Instances train = null;
    protected static Instances test = null;
    Detector() {
        super();
    }
    public void setTesting(String name) throws Exception {
        String filename = name;
        DataSource source_test = new DataSource(filename);
        test = source_test.getDataSet();
        if (test.classIndex() == -1) {
            test.setClassIndex(test.numAttributes()-1);
        }
    }
    public void loadModel(String name) throws Exception {
        Object[] model_and_data;
        model_and_data =  weka.core.SerializationHelper.readAll(name);
        m_classifier = (Classifier) model_and_data[0];
        train = (Instances) model_and_data[1];
    }
    public static void main (String[] args) throws Exception {
        Detector dt = new Detector();
        dt.loadModel("../Data_A.model");
        dt.setTesting("../Data_C_training.arff");
        Evaluation eval = new Evaluation(train);
        eval.evaluateModel(dt.m_classifier, test);
        System.out.println(eval.toSummaryString("\nResults\n==========\n", false));
        System.out.println(eval.toClassDetailsString("Score table\n===========\n"));
        System.out.println(eval.toMatrixString("Confusion Matrix\n================\n"));

    }
}
